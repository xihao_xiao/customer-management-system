<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOOT客户管理系统</title>
</head>
<body>

	<div class="top">
		<a style="color:#FFFFFF;margin:30px;font-size:30px";>BOOT客户管理系统</a>
		<ul class="nav">
			<li><a style="color:#E0E0E0; text-decoration:none;" href="${pageContext.request.contextPath}/logout">退出登录</a></li>
			<li><a style="color:#E0E0E0; text-decoration:none;">欢迎:   ${USER_SESSION.user_name}</a></li>
		</ul>
	</div>
	<div class="side">
		<ul class="sidenav">
			<li><a href="${pageContext.request.contextPath}/main">客户管理</a></li>
			<li><a href="${pageContext.request.contextPath}/main">客户拜访</a></li>
		</ul>
	</div>
	<div class="main">
	 <iframe name="main" style="width: 100%; min-height: 600px; border: 0;" src="${pageContext.request.contextPath}/select"></iframe>
	</div>
	<div class="footer"></div>



</body>
</html>