<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link  href="${pageContext.request.contextPath}/js/layui/css/layui.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/layui/layui.js"></script>




<script>

  layui.use('table', function(){
  var table = layui.table;
  table.render({
    elem: '#demo'
    ,height: 550
    ,url: '${pageContext.request.contextPath}/selectlist' //数据接口
    ,page:false
    ,limit:10
    ,cols: [[ //表头
      {field: 'cust_id', title: 'ID', width:80, sort: true, fixed: 'left'}
      ,{field: 'cust_name', title: '客户名称', width:100}
      ,{field: 'cust_source', title: '客户来源', width:120,sort: true}
      ,{field: 'cust_industry', title: '所属行业', width:100, sort: true}	
      ,{field: 'cust_level', title: '客户级别', width: 100,sort: true}
      ,{field: 'cust_phone', title: '固定电话', width: 150}
      ,{field: 'cust_moblie', title: '移动电话', width: 150}
      ,{field: 'right', title: '操作', width:177,toolbar:"#barDemo"}
     
    ]]
  ,parseData: function(res){ //res 即为原始返回的数据
      return {
          "code": 0, //解析接口状态
          "msg": "", //解析提示文本
          "count": res.length, //解析数据长度
          "data": res //解析数据列表
      };
  }
  });
  
}); 

</script>




<script>
function select(){
	  var cust_name=$('#c_name').val();
	  var cust_level=$('#c_level').val();
	  var cust_source=$('#c_source').val();
	  var cust_industry=$('#c_industry').val();
	 /*  $.ajax({
		  url:'${pageContext.request.contextPath}/selectlistByName?cust_name='+cust_name +'&cust_level='+cust_level+'&cust_source='+cust_source+'&cust_industry='+cust_industry,
		  contentType:"text/html;charset=UTF-8",
		  success:function (data){
			  alert(data);
		  }
		  
	  }); */
	 layui.use('table', function(){
		  var table = layui.table;
		  table.render({
		    elem: '#demo'
		    ,height: 550
		    ,url: '${pageContext.request.contextPath}/selectlistByName?cust_name='+cust_name+'&cust_level='+cust_level+'&cust_source='+cust_source+'&cust_industry='+cust_industry
		    ,page:false
		    ,limit:10
		    ,cols: [[ //表头
		      {field: 'cust_id', title: 'ID', width:80, sort: true, fixed: 'left'}
		      ,{field: 'cust_name', title: '客户名称', width:100}
		      ,{field: 'cust_source', title: '客户来源', width:120,sort: true}
		      ,{field: 'cust_industry', title: '所属行业', width:100, sort: true}	
		      ,{field: 'cust_level', title: '客户级别', width: 100,sort: true}
		      ,{field: 'cust_phone', title: '固定电话', width: 150}
		      ,{field: 'cust_moblie', title: '移动电话', width: 150}
		      ,{field: 'right', title: '操作', width:177,toolbar:"#barDemo"}
		     
		    ]]
		  ,parseData: function(res){ //res 即为原始返回的数据
		      return {
		          "code": 0, //解析接口状态
		          "msg": "", //解析提示文本
		          "count": res.length, //解析数据长度
		          "data": res //解析数据列表
		      };
		  }
		  });
		  
		}); 

	
}

</script>

<script>
	function add() {
		layer.open({
			type : 1,
			area : [ '500px', '500px' ],
			title : '添加客户信息',
			content : $("#add"),
			shade : 0,
			cancel : function(layero, index) {
				layer.closeAll();
			}
				
		});
		
	}
</script>
<script>
layui.use('table', function(){
	  var table = layui.table;
	table.on('tool(test)', function(obj) {
		if(obj.event === 'del'){
		      layer.confirm('真的删除行么', function(index){
		    	  $.ajax({
		    		  type:"post",
		    		  url:"${pageContext.request.contextPath}/delcustomer?cust_id="+obj.data.cust_id,
		    		  contentType:"text/html;charset=UTF-8",
		    		  success:function (){
		    		  }
		    	  });
		        obj.del();
		    	location.reload();
		        layer.close(index);
		      });
		    }else if(obj.event === 'edit'){
		    	$('#id').val(obj.data.cust_id);
		    	$('#name').val(obj.data.cust_name);
		    	$('#source').val(obj.data.cust_source);
		    	$('#industry').val(obj.data.cust_industry);
		    	$('#level').val(obj.data.cust_level);
		    	$('#phone').val(obj.data.cust_phone);
		    	$('#moblie').val(obj.data.cust_moblie);
		  
		    	
		    	
		    	
		    	layer.open({
					type : 1,
					area : [ '500px', '500px' ],
					title : '编辑客户信息',
					content : $("#edit"),
					shade : 0,
					cancel : function(layero, index) {
						layer.closeAll();
					}
						
				});
		    	
		    }
	})
})
</script>

<title>Boot客户管理系统</title>
</head>
<body>
<h1 style="font-size">客户信息</h1>



<div class="layui-row">

	<form accept-charset="utf-8">

		
		<label>客户来源：</label> 	
		<div class="layui-input-inline">
        <select lay-verify="required" lay-search="" id="c_source">
          <option value="">--客户来源--</option>
          <option value="网络营销">网络营销</option>
          <option value="电话营销">电话营销</option>
          
        </select>
      </div>

		
		
		<label>所属行业：</label>
		<div class="layui-input-inline">
        <select lay-verify="required" lay-search="" id="c_industry">
          <option value="">--所属行业--</option>
          <option value="电子商务">电子商务</option>
          <option value="国际贸易">国际贸易</option>
          <option value="互联网">互联网</option>
          
        </select>
      </div> 
		

		<label>客户级别：</label>
		<div class="layui-input-inline">
        <select lay-verify="required" lay-search="" id="c_level">
          <option value="">--客户级别--</option>
          <option value="vip">vip</option>
          <option value="普通用户">普通用户</option>
         
        </select>
      </div> 
		
		<label>客户名称：</label> 
		
		<input type="text" class="form-control" id="c_name" /> 

		<button onclick="select()">查询</button>
	</form>


</div>





<button id="add_btn" class="layui-btn layui-btn-sm" onclick="add();">添加客户信息</button>

<table id="demo" lay-filter="test"></table>


<!-- 添加隐藏表单 -->
	<table class="layui-hide" id="LAY_table_user" lay-filter="useruv"></table>

	<form class="layui-form" id="add" style="display: none" action="${pageContext.request.contextPath}/addcustomer.action" method="post" accept-charset="utf-8">
		<div class="layui-form-item">
			<label class="layui-form-label">客户名称</label>
			<div class="layui-input-block">
				<input type="text" name="cust_name" required lay-verify="required"
					placeholder="客户名称" autocomplete="on" class="layui-input"
					id="cust_name" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">客户来源</label>
			<div class="layui-input-block">
				<input type="text" name="cust_source" required lay-verify="required"
					placeholder="客户来源" autocomplete="on" class="layui-input"
					id="cust_source" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">所属行业</label>
			<div class="layui-input-block">
				<input type="text" name="cust_industry" required lay-verify="required"
					placeholder="请输入所属行业" autocomplete="on" class="layui-input"
					id="cust_industry" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">客户级别</label>
			<div class="layui-input-block">
				<input type="text" name="cust_level" required lay-verify="required"
					placeholder="请输入客户级别" autocomplete="on" class="layui-input"
					id="cust_level" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">固定电话</label>
			<div class="layui-input-block">
				<input type="text" name="cust_phone" required lay-verify="required"
					placeholder="固定电话" autocomplete="on" class="layui-input"
					id="cust_phone" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">移动电话</label>
			<div class="layui-input-block">
				<input type="text" name="cust_moblie" required lay-verify="required"
					placeholder="移动电话" autocomplete="on" class="layui-input"
					id="cust_moblie" style="width: 300px">
			</div>

		</div>



		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" class="layui-btn">提交</button>
				<button type="reset" class="layui-btn">取消</button>
			</div>
		</div>
	</form>

<!-- 编辑隐藏表单 -->

	<table class="layui-hide" id="LAY_table_user" lay-filter="useruv"></table>

	<form class="layui-form" id="edit" style="display: none"
		action="${pageContext.request.contextPath}/updatecustomer.action"
		method="post" accept-charset="utf-8"
		onsubmit="document.charset='utf-8'">
		<div class="layui-form-item">
			<label class="layui-form-label">客户ID</label>
			<div class="layui-input-block">
				<input type="text" name="id" required lay-verify="required"
					autocomplete="on" class="layui-input"
					id="id" style="width: 300px" readonly="readonly">
			</div>

		</div>
		
		<div class="layui-form-item">
			<label class="layui-form-label">客户名称</label>
			<div class="layui-input-block">
				<input type="text" name="name" required lay-verify="required"
					 autocomplete="on" class="layui-input"
					id="name" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">客户来源</label>
			<div class="layui-input-block">
				<input type="text" name="source" required lay-verify="required"
					 autocomplete="on" class="layui-input"
					id="source" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">所属行业</label>
			<div class="layui-input-block">
				<input type="text" name="industry" required
					lay-verify="required"  autocomplete="on"
					class="layui-input" id="industry" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">客户级别</label>
			<div class="layui-input-block">
				<input type="text" name="level" required lay-verify="required"
					 autocomplete="on" class="layui-input"
					id="level" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">固定电话</label>
			<div class="layui-input-block">
				<input type="text" name="phone" required lay-verify="required"
					autocomplete="on" class="layui-input"
					id="phone" style="width: 300px">
			</div>

		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">移动电话</label>
			<div class="layui-input-block">
				<input type="text" name="moblie" required lay-verify="required"
					 autocomplete="on" class="layui-input"
					id="moblie" style="width: 300px">
			</div>

		</div>



		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" class="layui-btn">提交</button>
				<button type="reset" class="layui-btn">取消</button>
			</div>
		</div>
	</form>

	<script type="text/html" id="barDemo">
    	<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="edit">编辑</a>
    	<a class="layui-btn layui-btn-xs " lay-event="del">删除</a>
	</script>


</body>
