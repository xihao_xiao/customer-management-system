<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet"/>
<title>登录</title>
</head>
<body style="background-color:#F0FFFF">
	<div id="main" align="center"style="padding-top:100px">
		<div id="tiele">
			<h1>BOOT客户管理系统</h1>
		</div>
		
		<div id="login" style="border:3px solid #B0C4DE;width:500px;height:220px;background-color:#FFFFFF;">
			<h2 style="color:#B0C4DE;font-size:25px">用户登录</h2>
			<form action="${pageContext.request.contextPath}/login.action" method="post">
				<table style="border-collapse:separate; border-spacing:5px 10px;">
					<tr>
						<td><a style="color:#B0C4DE;font-size:20px">账号:</a></td>
						<td><input id="usercode" type="text" name="usercode" style="height:22px;width:200px;border: 2px solid #B0C4DE;"></td>
					</tr>
					<tr>
						<td><a style="color:#B0C4DE;font-size:20px">密码:</a>
						<td><input id="password" type="password" name="password" style="height:22px;width:200px;border: 2px solid #B0C4DE;"></td>
					</tr>
				</table>
			<button class="button1" type="submit">登录</button>
			<button class="button1" type="reset">取消</button>
			
			</form>
			<a>${msg}</a>
		</div>

	</div>




</body>
</html>