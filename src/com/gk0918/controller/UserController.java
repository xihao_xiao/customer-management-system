package com.gk0918.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.gk0918.po.User;
import com.gk0918.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userservice;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(String usercode, String password, Model model, HttpSession session) {
		User user = this.userservice.findUser(usercode, password);
		if (user != null) {
			session.setAttribute("USER_SESSION", user);

			return "main";
		}
		model.addAttribute("msg", "密码或者账号有误,请重新输入");

		return "login";

	}

	@RequestMapping(value = "main")
	public String toMain() {

		return "main";
	}

	@RequestMapping(value = "logout")
	public String logout(HttpSession session) {
		session.invalidate();

		return "login";

	}


	
	

}
