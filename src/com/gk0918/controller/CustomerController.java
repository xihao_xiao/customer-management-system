package com.gk0918.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gk0918.po.Customer;
import com.gk0918.service.CustomerService;
@Controller
public class CustomerController {
	@Autowired
	private CustomerService CustomerService;
	
	@RequestMapping(value="selectlist")
	@ResponseBody
	public List<Customer> selectCustomerList() {
		
		List<Customer> customerlist=CustomerService.selectCustomerList();
		
		return customerlist;
	}
	
	@RequestMapping(value="select")
	public String toList() {
		
		return "select";
	}
	
	@RequestMapping(value="addcustomer",method = RequestMethod.POST)
	public String addCustomer(String cust_name,String cust_source,String cust_industry,String cust_level,String cust_phone,String cust_moblie) {
		Customer Customer=new Customer();
		
		Customer.setCust_name(cust_name);
		Customer.setCust_source(cust_source);
		Customer.setCust_industry(cust_industry);
		Customer.setCust_level(cust_level);
		Customer.setCust_phone(cust_phone);
		Customer.setCust_moblie(cust_moblie);
		CustomerService.addCustomer(Customer);
		return "select";
		
	}
	
	@RequestMapping(value="delcustomer")
	public void delcustomer(Integer cust_id) {
		CustomerService.delCustomerById(cust_id);
	}
	
	@RequestMapping(value="updatecustomer",method = RequestMethod.POST)
	public String updatecustomer(Integer id,String name,String source,String industry,String level,String phone,String moblie) {
		Customer Customer=new Customer();
		Customer.setCust_id(id);
		Customer.setCust_name(name);
		Customer.setCust_source(source);
		Customer.setCust_industry(industry);
		Customer.setCust_level(level);
		Customer.setCust_phone(phone);
		Customer.setCust_moblie(moblie);
		System.out.println(Customer);
		CustomerService.updateCustomerById(Customer);
		
		return"select";
		
	}
	@RequestMapping(value="selectlistByName")
	@ResponseBody
	public List<Customer> selectCustomerList(Customer c){
		List<Customer> customerlist=CustomerService.selectCustomerListByname(c);
		System.out.println(customerlist);
		return customerlist;
	}
	
}
