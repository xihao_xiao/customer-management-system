package com.gk0918.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gk0918.dao.*;
import com.gk0918.po.User;
import com.gk0918.service.UserService;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao UserDao;
	@Override
	public User findUser(String username, String password) {
		User user = this.UserDao.findUser(username, password);
		
		
		return user;
	}

}
