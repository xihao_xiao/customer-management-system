package com.gk0918.service.impl;

import java.util.List;

import org.apache.tools.ant.types.resources.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk0918.po.Customer;
import com.gk0918.service.CustomerService;
import com.gk0918.dao.CustomerDao;


@Service("CustomerService")
@Transactional
public class CustomerServiceImple implements CustomerService {
	@Autowired
	private CustomerDao CustomerDao;
	@Override
	public List<Customer> selectCustomerList() {
		List<Customer> customerlist = this.CustomerDao.selectCustomerList();
		return customerlist;
	}
	@Override
	public Customer addCustomer(Customer customer) {
		CustomerDao.addCustomer(customer);
		return null;
	}
	@Override
	public int delCustomerById(int cust_id) {
		CustomerDao.delCustomerById(cust_id);
		return 0;
	}
	@Override
	public int updateCustomerById(Customer customer) {
		CustomerDao.updateCustomerById(customer);
		return 0;
	}
	@Override
	public List<Customer> selectCustomerListByname(Customer c) {
		List<Customer> customerlist=CustomerDao.selectCustomerListByname(c);
		System.out.println(customerlist);
		return customerlist;
	}

}
