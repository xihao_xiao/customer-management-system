package com.gk0918.service;

import com.gk0918.po.User;

public interface UserService {
	public User findUser(String usercode,String password);

}
