package com.gk0918.service;

import java.util.List;

import com.gk0918.po.Customer;

public interface CustomerService {
	public List<Customer> selectCustomerList();
	public Customer addCustomer(Customer customer);
	public int delCustomerById(int cust_id);
	public int updateCustomerById(Customer customer);
	public List<Customer> selectCustomerListByname(Customer c);

}
