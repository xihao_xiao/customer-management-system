package com.gk0918.dao;

import org.apache.ibatis.annotations.Param;

import com.gk0918.po.User;

public interface UserDao {
	public User findUser(@Param("usercode") String usercode,
						 @Param("password") String password);
	

}

