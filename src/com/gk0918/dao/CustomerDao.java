package com.gk0918.dao;

import java.util.List;

import com.gk0918.po.Customer;

public interface CustomerDao {
	public List<Customer> selectCustomerList();
	public int addCustomer(Customer customer);
	public int delCustomerById(int cust_id);
	public int updateCustomerById(Customer customer);
	public List<Customer> selectCustomerListByname(Customer c);
	

}
